import unittest

from classe.demo import Demo

class TestDemo(unittest.TestCase):
    
    def setUp(self) -> None:
        # Se lance à chaque test
        self.demo = Demo()
        
    def tearDown(self) -> None:
        del self.demo
    
    def test_add(self):
        self.assertEqual(self.demo.add(1, 3), 4, "0 n'est pas égal à 4")
        
        with self.assertRaises(Exception) as context:
            self.demo.add("m",45)
        self.assertTrue("error" in str(context.exception))